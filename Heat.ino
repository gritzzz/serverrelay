
#include "DHT.h"
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

#define DHTPIN D2     // Digital pin connected to the DHT sensor

#define DHTTYPE DHT11   // DHT 22  (AM2302), AM2321

DHT dht(DHTPIN, DHTTYPE);

#ifndef STASSID
#define STASSID "Dom_2.4G"
#define STAPSK "Homagrus1"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;
const int relay = D1;

float maxTemperature = 30.0;
float minTemperature = 15.0;

unsigned long previousMillis = 0;
const long relayInterval = 5000;

int relayStatus = LOW;

ESP8266WebServer server(80);

void handleRoot() {
  server.send(200, "text/plain", "hello from esp8266!\r\n");
}
void controlRelay(float t) {
  int newRelayStatus = relayStatus;

  if (t > maxTemperature) {
    newRelayStatus = LOW;  // Выключаем реле, если температура превышает максимальное значение
    
  } else if (t < minTemperature) {
    newRelayStatus = HIGH; // Включаем реле, если температура ниже минимального значения
    
  }
    // Проверяем изменение статуса и устанавливаем новый статус, если нужно
  if (newRelayStatus != relayStatus) {
    digitalWrite(relay, newRelayStatus);
    Serial.println(F("Relay:"));
    Serial.println(newRelayStatus);
    relayStatus = newRelayStatus;
  }
}
void handleSetTemperature() {
  // Проверяем, что пришли данные JSON
  if (server.hasArg("plain")) {
    String jsonStr = server.arg("plain");
    
    // Разбираем JSON
    DynamicJsonDocument jsonDocument(128);
    DeserializationError error = deserializeJson(jsonDocument, jsonStr);
    
    // Проверяем наличие ошибок при разборе JSON
    if (!error) {
      // Извлекаем значения maxtemperature и mintemperature
      float newMaxTemperature = jsonDocument["maxtemperature"];
      float newMinTemperature = jsonDocument["mintemperature"];
      
      // Обновляем значения переменных
      maxTemperature = newMaxTemperature;
      minTemperature = newMinTemperature;
      
      // Отправляем ответ
      server.send(200, "text/plain", "Temperature settings updated");
      
      Serial.print(F("Max Temperature updated to: "));
      Serial.println(maxTemperature);
      Serial.print(F("Min Temperature updated to: "));
      Serial.println(minTemperature);
      
      return;
    }
  }
  
  // Если пришли некорректные данные, отправляем ошибку
  server.send(400, "text/plain", "Bad Request");
}

void handleRelayStatus() {
  // Отправляем JSON-ответ с текущим статусом реле (true или false)
  bool isRelayOn = (relayStatus == LOW) ? false : true;
  String jsonStr = "{\"relayStatus\":" + String(isRelayOn) + "}";
  server.send(200, "application/json", jsonStr);
}

void handleTemperature() {
  // Создаем JSON-документ
  StaticJsonDocument<64> jsonDocument;
  float h=dht.readHumidity();
  float t=dht.readTemperature();
  jsonDocument["temperature"] = t;
  jsonDocument["humidity"] = h;


  // Сериализуем JSON в строку
  String jsonString;
  serializeJson(jsonDocument, jsonString);

  // Отправляем JSON-ответ
  server.send(200, "application/json", jsonString);
  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));


}

void setup() {
  Serial.begin(9600);
 
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // Обработчики маршрутов
  server.on("/", handleRoot);
  server.on("/gettemperature", HTTP_GET, handleTemperature);
  server.on("/settemperature", HTTP_POST, handleSetTemperature);
  server.on("/relaystatus", HTTP_GET, handleRelayStatus);

  // Запуск сервера
  server.begin();
  Serial.println("HTTP server started");

  dht.begin();
  pinMode(relay,OUTPUT);
}

void loop() {
  server.handleClient();
  
  unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= relayInterval) {
    previousMillis = currentMillis;
    float currentTemperature = dht.readTemperature();
  controlRelay(currentTemperature);
    }
}
